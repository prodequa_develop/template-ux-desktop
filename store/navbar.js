const state = () => ({
  menu: [
    {
      title: 'INICIO',
      ref: '/',
      type: 'text',
      enablechildren: false,
      children: []
    },
    {
      title: '¿Quieres<br>Conocernos?',
      ref: '/nosotros',
      type: 'text',
      enablechildren: true,
      children: [
        {
          title: 'LINKEDIN',
          ref: '/nosotros/porque-usar-linkedin'
        },
        {
          title: 'SOBRE MI',
          ref: '/nosotros/conoce-sobre-mi'
        },
        {
          title: '¿QUÉ HACEMOS?',
          ref: '/nosotros/que-hacemos'
        },
        {
          title: 'NUESTROS CLIENTES',
          ref: '/nosotros/nuestros-clientes'
        }
      ]
    },
    {
      title: 'Nuestros<br>Productos',
      ref: '/productos',
      type: 'text',
      enablechildren: false,
      children: []
    },
    {
      title: 'Nuestros<br>Servicios',
      ref: '/servicios',
      type: 'text',
      enablechildren: false,
      children: []
    },
    {
      title: 'Eventos',
      ref: '/eventos',
      type: 'text',
      enablechildren: false,
      children: []
    },
    {
      title: 'Modal',
      ref: '/modal',
      type: 'text',
      enablechildren: false,
      children: []
    },
    {
      title: 'Contáctanos',
      ref: '/contactanos',
      type: 'text',
      enablechildren: false,
      children: []
    }
  ]
})

export default {
  state
}
